
const FIRST_NAME = "Andrada";
const LAST_NAME = "Alecu";
const GRUPA = "1090";




 
function numberParser(value) {
   
    if(value>Number.MAX_SAFE_INTEGER||value<Number.MIN_SAFE_INTEGER)
    return NaN;
    if(value===NaN)
    return NaN;
  
    return parseInt(value);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

